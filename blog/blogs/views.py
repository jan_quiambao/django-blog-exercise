from django.shortcuts   import get_object_or_404, render
from django.urls        import reverse
from django.http        import HttpResponseRedirect

from .models import User

def index(request):
    if 'error_message' in request.session.keys(): del request.session['error_message'] 
    return render(request, 'blogs/index.html')

def signuppage(request):
    return render(request, 'blogs/signup.html')

def welcome(request):
    return render(request, 'blogs/welcome.html')

def signin(request):
    errorScenario = render(request, 'blogs/index.html', { 'error_message' : "Incorrect Username/Password!"})
    try:
        user = User.objects.get(pk = request.POST['user_name'])
    except(KeyError, User.DoesNotExist):
        return errorScenario
    else:
        if user.password == request.POST['password']:
            request.session["user_name"] = user.user_name
            return HttpResponseRedirect(reverse('blogs:welcome'))
        else:
            return errorScenario

def signup(request):
    if User.objects.filter(user_name=request.POST['user_name']).exists():
        request.session['error_message'] = "Username already exists!"
        return HttpResponseRedirect(reverse('blogs:signuppage'))
    else:
        User.objects.create(user_name=request.POST['user_name'], password=request.POST['password'], user_type='U')
        request.session["message"] = "Sign-Up Success!"
        return HttpResponseRedirect(reverse('blogs:index'))
