from django.urls import path

from . import views

app_name = 'blogs'
urlpatterns = [
    path('', views.index, name = 'index'),
    path('signin/', views.signin, name = 'signin'),
    path('signup/', views.signup, name =' signup'),
    path('signuppage/', views.signuppage, name = 'signuppage'),
    path('welcome/', views.welcome, name = 'welcome'),
]