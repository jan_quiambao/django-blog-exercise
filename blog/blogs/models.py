from django.db         import models
from django.utils.html import format_html

class User(models.Model):
    USER_TYPES = (
        ('A', 'Administrator'),
        ('U', 'User'),
    )

    user_name = models.CharField(max_length = 20, primary_key = True)
    #TODO: Add protection against SQL Injection
    password  = models.CharField(max_length = 20)
    user_type = models.CharField(max_length = 1, choices = USER_TYPES)

    def __str__(self):
        return format_html('User Name: %s <br> Password: %s <br> User_type: %s' % (self.user_name,
                                                                                   self.password,
                                                                                   self.user_type))

    #TODO: Check if this function has any relevance
    def is_user_an_admin(self):
        return self.user_type == 'A'

