# Welcome to Blog Application! (WIP)

## Overview of Contents
* The __blog/__ directory contains all source code used in developing the Blog application.
* The __requirements.txt__ contains all libraries and dependencies used.

## Breakdown of Pages and their respective Functionality
1. __Sign-In__ - allows the user to sign-in using his/her username and password.
2. __Sign-Up__ - allows the user to register his/her uniquie username and password to the database.
3. __Welcome__ - if the user is able to successfully sign, they will be redirected to this page welcoming them to the Blog.
4. __Create Post__ - allows the user to write his/her own post

## Developed using:
* Django
* Python
* HTML
* PostgreSQL (Database)

\**Note: More functionality to be implemented...*